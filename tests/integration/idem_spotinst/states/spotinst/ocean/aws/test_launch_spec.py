import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_launch_spec(hub, ctx, aws_image, ocean_cluster):
    launch_spec_temp_name = "idem-test-launch-spec-" + str(uuid.uuid4())
    image_id = aws_image
    ocean_id = ocean_cluster
    instance_types = ["c5.large"]

    # create launch_spec with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.spotinst.ocean.aws.launch_spec.present(
        test_ctx,
        name=launch_spec_temp_name,
        image_id=image_id,
        ocean_id=ocean_id,
        instance_types=instance_types,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create spotinst.ocean.aws.launch_spec '{launch_spec_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_launch_spec_states(
        image_id, instance_types, launch_spec_temp_name, ocean_id, resource
    )

    # create launch_spec in real
    ret = await hub.states.spotinst.ocean.aws.launch_spec.present(
        ctx,
        name=launch_spec_temp_name,
        image_id=image_id,
        ocean_id=ocean_id,
        instance_types=instance_types,
    )
    assert ret["result"], ret["comment"]

    assert (
        f"Created spotinst.ocean.aws.launch_spec '{launch_spec_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_launch_spec_states(
        image_id, instance_types, launch_spec_temp_name, ocean_id, resource
    )
    resource_id = resource.get("resource_id")

    # Describe launch_spec
    describe_ret = await hub.states.spotinst.ocean.aws.launch_spec.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "spotinst.ocean.aws.launch_spec.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "spotinst.ocean.aws.launch_spec.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_launch_spec_states(
        image_id,
        instance_types,
        launch_spec_temp_name,
        ocean_id,
        described_resource_map,
    )
    assert resource_id == described_resource_map.get("resource_id")

    # update launch_spec with test flag
    updated_instance_types = ["t3.micro"]
    ret = await hub.states.spotinst.ocean.aws.launch_spec.present(
        test_ctx,
        name=launch_spec_temp_name,
        resource_id=resource_id,
        image_id=image_id,
        ocean_id=ocean_id,
        instance_types=updated_instance_types,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Would update spotinst.ocean.aws.launch_spec '{resource_id}'" in ret["comment"]
    )
    resource = ret.get("old_state")
    assert_launch_spec_states(
        image_id, instance_types, launch_spec_temp_name, ocean_id, resource
    )

    resource = ret.get("new_state")
    assert_launch_spec_states(
        image_id, updated_instance_types, launch_spec_temp_name, ocean_id, resource
    )

    # update launch_spec
    ret = await hub.states.spotinst.ocean.aws.launch_spec.present(
        ctx,
        name=launch_spec_temp_name,
        resource_id=resource_id,
        image_id=image_id,
        ocean_id=ocean_id,
        instance_types=updated_instance_types,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"Updated spotinst.ocean.aws.launch_spec Id: '{resource_id}')" in ret["comment"]
    )
    resource = ret.get("old_state")
    assert_launch_spec_states(
        image_id, instance_types, launch_spec_temp_name, ocean_id, resource
    )

    resource = ret.get("new_state")
    assert_launch_spec_states(
        image_id, updated_instance_types, launch_spec_temp_name, ocean_id, resource
    )

    # Simulate ignore_change - If a parameter is specified under the ignore_changes and parameter will be overridden
    # with None and present() function will ignore updating such None-value parameters
    ret = await hub.states.spotinst.ocean.aws.launch_spec.present(
        ctx,
        name=launch_spec_temp_name,
        resource_id=resource_id,
        image_id=image_id,
        ocean_id=ocean_id,
        instance_types=None,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"spotinst.ocean.aws.launch_spec '{launch_spec_temp_name}'(resource Id: '{resource_id}') already present"
        in ret["comment"]
    )

    resource = ret.get("old_state")
    assert_launch_spec_states(
        image_id, updated_instance_types, launch_spec_temp_name, ocean_id, resource
    )

    resource = ret.get("new_state")
    assert_launch_spec_states(
        image_id, updated_instance_types, launch_spec_temp_name, ocean_id, resource
    )

    # Delete launch_spec with test flag
    ret = await hub.states.spotinst.ocean.aws.launch_spec.absent(
        test_ctx, name=launch_spec_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete spotinst.ocean.aws.launch_spec '{launch_spec_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert_launch_spec_states(
        image_id, updated_instance_types, launch_spec_temp_name, ocean_id, resource
    )

    # Delete launch_spec
    ret = await hub.states.spotinst.ocean.aws.launch_spec.absent(
        ctx, name=launch_spec_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted spotinst.ocean.aws.launch_spec '{launch_spec_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert_launch_spec_states(
        image_id, updated_instance_types, launch_spec_temp_name, ocean_id, resource
    )

    # Deleting launch_spec again should be an no-op
    ret = await hub.states.spotinst.ocean.aws.launch_spec.absent(
        ctx, name=launch_spec_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"spotinst.ocean.aws.launch_spec '{launch_spec_temp_name}' already absent"
        in ret["comment"]
    )


def assert_launch_spec_states(
    image_id, instance_types, launch_spec_temp_name, ocean_id, resource
):
    assert launch_spec_temp_name == resource.get("name")
    assert ocean_id == resource.get("ocean_id")
    assert image_id == resource.get("image_id")
    assert instance_types == resource.get("instance_types")
