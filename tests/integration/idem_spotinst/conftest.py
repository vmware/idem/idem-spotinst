import copy
import os
import random
import uuid
from typing import Any
from typing import Dict

import pytest_asyncio


# ================================================================================
# AWS resource fixtures
# ================================================================================
@pytest_asyncio.fixture(scope="module")
async def aws_ec2_vpc(hub, aws_ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 vpc for a module that needs it
    :return: a description of an ec2 vpc
    """
    vpc_temp_name = "idem-ocean-fixture-vpc-" + str(uuid.uuid4())
    cidr_block = os.getenv("IT_FIXTURE_EC2_VPC_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"192.168.{num()}.0/24"
    cidr_block_association_set = [{"CidrBlock": cidr_block}]
    ret = await hub.states.aws.ec2.vpc.present(
        aws_ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=[{"Key": "Name", "Value": vpc_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")

    resource = hub.tool.boto3.resource.create(
        aws_ctx, "ec2", "Vpc", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)
    assert after

    yield after

    ret = await hub.states.aws.ec2.vpc.absent(
        aws_ctx, name=vpc_temp_name, resource_id=after["VpcId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_security_group(hub, aws_ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 security group for a module that needs it
    :return: a description of an ec2 security group
    """
    security_group_temp_name = "idem-ocean-fixture-security-group-" + str(uuid.uuid4())
    security_group_tags = [{"Key": "Name", "Value": security_group_temp_name}]
    description = "Security group fixture resource for Idem integration test."
    ret = await hub.states.aws.ec2.security_group.present(
        aws_ctx,
        name=security_group_temp_name,
        description=description,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=security_group_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.security_group.absent(
        aws_ctx, name=security_group_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_subnet(hub, aws_ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-ocean-fixture-subnet-" + str(uuid.uuid4())
    az = aws_ctx["acct"].get("region_name") + "b"
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc.get("CidrBlock"), 28
    )
    ret = await hub.states.aws.ec2.subnet.present(
        aws_ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        availability_zone=az,
        tags=[{"Key": "Name", "Value": subnet_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = hub.tool.boto3.resource.create(
        aws_ctx, "ec2", "Subnet", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)

    yield after

    resource = await hub.exec.boto3.client.ec2.describe_instances(
        aws_ctx,
        Filters=[{"Name": "vpc-id", "Values": [aws_ec2_vpc.get("VpcId")]}],
    )

    await hub.exec.boto3.client.ec2.terminate_instances(
        aws_ctx,
        InstanceIds=[
            resource["ret"].get("Reservations")[0].get("Instances")[0].get("InstanceId")
        ],
    )
    await hub.tool.boto3.client.wait(
        aws_ctx,
        "ec2",
        "instance_terminated",
        InstanceIds=[
            resource["ret"].get("Reservations")[0].get("Instances")[0].get("InstanceId")
        ],
    )
    ret = await hub.states.aws.ec2.subnet.absent(
        aws_ctx, name=subnet_temp_name, resource_id=after["SubnetId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_image(hub, aws_ctx) -> str:
    """
    Fetch ami image id
    :return: image id
    """

    filters = [
        {"Name": "owner-alias", "Values": ["amazon"]},
        {"Name": "image-type", "Values": ["machine"]},
        {"Name": "architecture", "Values": ["x86_64"]},
        {"Name": "platform", "Values": ["windows"]},
        {"Name": "state", "Values": ["available"]},
        {"Name": "virtualization-type", "Values": ["hvm"]},
    ]
    resource = await hub.exec.boto3.client.ec2.describe_images(
        aws_ctx,
        Filters=filters,
        IncludeDeprecated=False,
        DryRun=False,
    )

    image_id = resource.ret["Images"][0]["ImageId"]
    yield image_id


@pytest_asyncio.fixture(scope="module")
async def ocean_cluster(
    hub, ctx, aws_ec2_security_group, aws_ctx, aws_ec2_subnet, aws_image
) -> str:
    """
    Create and cleanup a spotinst ocean cluster for a module that needs it
    :return: ocean cluster id
    """
    k8s_cluster_temp_name = "idem-test-k8s-cluster-" + str(uuid.uuid4())
    region = aws_ctx.acct.get("region_name")
    subnet_ids = [aws_ec2_subnet.get("SubnetId")]
    security_group_id = aws_ec2_security_group.get("resource_id")
    compute = {
        "subnetIds": subnet_ids,
        "launchSpecification": {
            "imageId": aws_image,
            "securityGroupIds": [security_group_id],
        },
    }
    controller_cluster_id = "ocean.k8s-11" + str(uuid.uuid4())
    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.present(
        ctx,
        name=k8s_cluster_temp_name,
        controller_cluster_id=controller_cluster_id,
        region=region,
        compute=compute,
    )
    assert ret["result"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    yield resource_id

    ret = await hub.states.spotinst.ocean.aws.k8s_cluster.absent(
        ctx, name=k8s_cluster_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
