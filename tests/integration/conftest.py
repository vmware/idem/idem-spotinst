import asyncio
import copy
import sys
import unittest.mock as mock
from typing import Any
from typing import Dict
from typing import List

import dict_tools
import pop.hub
import pytest
import pytest_asyncio

from tests import api_data as data


# ================================================================================
# pop fixtures
# ================================================================================
@pytest.fixture(scope="module", autouse=True, name="acct_subs")
def acct_subs() -> List[str]:
    return ["spotinst"]


@pytest.fixture(scope="module", autouse=True, name="aws_acct_subs")
def aws_acct_subs() -> List[str]:
    return ["aws"]


@pytest.fixture(scope="module", autouse=True)
def acct_profile() -> str:
    return "test_development_spotinst_aws"


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    try:
        yield loop
    finally:
        loop.close()


@pytest.fixture(scope="module", name="hub")
def tpath_hub(code_dir, event_loop):
    """
    Add "idem_plugin" to the test path
    """
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        hub.pop.sub.add(dyne_name="idem")
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=False)

        yield hub


@pytest_asyncio.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)

    # Add the profile to the account
    if hub.OPT.acct.acct_file and hub.OPT.acct.acct_key:
        await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
        ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)
    else:
        hub.log.warning("No credentials found, considering below spot account")
        ctx.acct = dict(
            account_id="act-d60b6ecd",
            token="b5460afe3c29a30c28abd54d190d1aa923587574321e75925cfc8268b54bda4f",
        )

    yield ctx


@pytest_asyncio.fixture(scope="module", name="aws_ctx")
async def integration_aws_ctx(
    hub, aws_acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    aws_ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)

    # Add the profile to the account
    if hub.OPT.acct.acct_file and hub.OPT.acct.acct_key:
        await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
        aws_ctx.acct = await hub.acct.init.gather(aws_acct_subs, acct_profile)
    else:
        hub.log.warning("No credentials found, assuming localstack on localhost")
        aws_ctx.acct = dict(
            region_name="us-west-2",
            endpoint_url="http://0.0.0.0:4566",
            aws_access_key_id="localstack",
            aws_secret_access_key="localstack",
        )

    yield aws_ctx


# --------------------------------------------------------------------------------


@pytest.fixture(scope="function")
def url(hub):
    url = copy.deepcopy(data.URL)
    return url


@pytest.fixture(scope="function")
def desc(hub):
    desc = copy.deepcopy(data.DESC)
    return desc
